#!/bin/bash

# Check if the role already exists
role_exists=$(psql -U "$POSTGRES_USER" -d "$POSTGRES_DB" -tAc "SELECT 1 FROM pg_roles WHERE rolname='$POSTGRES_USER'")

# If the role doesn't exist, create it
if [ -z "$role_exists" ]; then
  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" -d "$POSTGRES_DB" <<-EOSQL
    CREATE USER "$POSTGRES_USER" WITH ENCRYPTED PASSWORD '$POSTGRES_PASSWORD';
EOSQL
fi

# Check if the database already exists
db_exists=$(psql -U "$POSTGRES_USER" -d postgres -tAc "SELECT 1 FROM pg_database WHERE datname='$POSTGRES_DB'")

# If the database doesn't exist, create it
if [ -z "$db_exists" ]; then
  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" -d postgres <<-EOSQL
    CREATE DATABASE "$POSTGRES_DB" OWNER "$POSTGRES_USER";
EOSQL
fi

# Create the necessary table
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" -d "$POSTGRES_DB" <<-EOSQL
    CREATE TABLE IF NOT EXISTS task (id SERIAL PRIMARY KEY NOT NULL, task text UNIQUE, status INTEGER DEFAULT 0);
EOSQL
